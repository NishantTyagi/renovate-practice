module.exports = {
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    token: process.env.RENOVATE_TOKEN,

    logLevel: 'info',
    onboarding: true,
    onboardingConfig: {
        extends: [
            'config:base',
        ]
    },

    enabledManagers: [
        'npm',
    ],
};